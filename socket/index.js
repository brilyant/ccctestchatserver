const sockets = require('socket.io')
const redis = require('socket.io-redis')

const cp = require('child_process')

const bindListeners = (io) => {
    io.on('connection', (socket) => {
    })
}

module.exports = function(app, http) {      
    const io = sockets(http, {
        transports: [ 'websocket', 'polling' ]
    })
    // Add the redis adapter
    io.adapter(redis({ host: 'localhost', port: 6379 }))   
    bindListeners(io)

    app.get('/socket', function(req, res){
        res.sendFile(__dirname + '/index.html')
    })


    io.use(function(socket, next){
        if (socket.handshake.query && socket.handshake.query.token){
            // eslint-disable-next-line no-undef
            const result = globallibs.authorizationapi.defaultVerify(socket.handshake.query.token)
            if (result){
                socket.decoded = result
                next()
            } else {
                return next(new Error('Authentication error (Token Not Valid)'))
            }
        } else {
            next(new Error('Authentication error (Token Not Specified)'))
        }    
    }).on('connection', function(socket){
        // console.log('a user connected'+` (${socket.handshake.query.token})`)

        //Join
        // socket.join(socket.handshake.query.token)  
        io.of('/').adapter.remoteJoin(socket.id, socket.handshake.query.token, (err) => {
            if (err) { /* unknown id */ }
            // success
            console.log(socket.id, socket.handshake.query.token, 'is joined')
        })
         

        //Experiment Area
        socket.on('doPerhitungan', function(msg){
            console.log('parameter identity: ' + msg)
            const totalBanyakData = 99900009999
            
            const maxCPU = require('os').cpus().length
            // console.log('maxCPU / totalMaxProses: ' + maxCPU)
            const totalMaxProses = maxCPU

            let prosesDataKelipatan = Math.ceil((totalBanyakData/totalMaxProses))
        
            const chunkByDefinedValue = chunkBy(prosesDataKelipatan)
            const arrayData = chunkByDefinedValue(totalBanyakData)


            // socket.emit('chat message', arrayData)
            let counter = 1
            let n = null    
            for (let i = 0; i < totalMaxProses; i++) {  
                n/*[i]*/ = cp.fork('./worker/ProsesSample.js')
                n/*[i]*/.on('message', function(m) {
                    // if (m.error) {
                    //     for (let j = 0; j < totalMaxProses; j++) {
                    //         n[j].kill('SIGKILL')
                    //     }
                    //     console.log('error worker', m.message)   
                    //     socket.emit('chat message', m.message)
                    // }   
                    // io.emit('resultprogress', m ) 

                    if (m.done){
                        const msg = `(${m.identity}) No. Proses[${m.noProses}] Persentase : ${( (100 / totalMaxProses) * (counter) )}`
                        counter++ 
                        io.emit('resultprogress', msg) 
                        // io.in(socket.handshake.query.token).emit('resultprogress', msg) //ini untuk per room(dipakai saat production) 
                    } else {
                        io.emit('resultprogress', m)
                        // io.in(socket.handshake.query.token).emit('resultprogress', msg)
                    }
                }) 
                n/*[i]*/.send({ action: 'run', param: { noProses: i, loopCount: arrayData[i], totalMaxProses, identity: msg } })        
            } 

        })
        //END - Experiment Area

        socket.on('disconnect', function(){ //manda
            //Leave 
            // socket.leave(socket.handshake.query.token) 
            io.of('/').adapter.remoteLeave(socket.id, socket.handshake.query.token, (err) => {
                if (err) { /* unknown id */ }
                // success
                // console.log(socket.id, socket.handshake.query.token, 'is LEAVE')
            })
            // console.log('user disconnected'+` (${socket.handshake.query.token})`)
        })

        socket.on('error', (error) => {
            console.log('socket', error)
            socket.emit('systeminfo', error)
        })
    })

    //CUSTOM FUNCTION

    const chunkBy = (n) => number => {
        var chunks = new Array(Math.floor(number / n)).fill(n)
        var remainder = number % n
        // console.log('CHUNKS = ', chunks)
        if (remainder > 0) {
            chunks.push(remainder)
        }
      
        return chunks
    }
    
}