//Define PORT
const PORT = 5444

// importing the dependencies
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const helmet = require('helmet')
const ip = require('ip')
const moment = require('moment')
const _ = require('lodash')

//MONGO DB
const MongoClient = require('mongodb').MongoClient
 
// defining the Express app
const app = express()

//MIDDLEWARE
// adding Helmet to enhance your API's security
app.use(helmet()) 
// using bodyParser to parse JSON bodies into JS objects
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
// enabling CORS for all requests
app.use(cors())
 
// starting the server
const server = app.listen(PORT, () => {
    console.log(`[${ip.address()}] listening on port ${PORT}`) 
})
var io = require('socket.io').listen(server)

//Socket
//require('./socket')(app, server)

app.get('/', function (req, res) {
    res.send('Hai ...')
})

const url = 'mongodb://localhost'

MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }, function(err, client) {
    var db = client.db('ChatDB')
    console.log('mongodb up')

    app.post('/testinsert', function (req, res) {
        const parameter = req.body

        console.log(parameter)

        db.collection('Perbincangan').insertOne(parameter)

        res.send('Berhasil DI Insert')
    })

    io.on("connection", socket =>{
        console.log('socket up')
        //test
        app.get('/kirim/:room', function (req, res) { 
            const { room } = req.params
            console.log('kirim ke', room)
    
            socket.to(room).emit('fromserver', 'HAI')

            res.send('OK: '+room)
        })
        //end test
 
        let userobject //= (socket.handshake['query']['userobject'] !== undefined) ? JSON.parse(socket.handshake['query']['userobject']) : null
        if (socket.handshake['query']) {
            if (socket.handshake['query']['userobject']) {
                if (socket.handshake['query']['userobject']) { 
                    if (socket.handshake['query']['userobject'] === 'global') {
                        socket.join('global') 
                    } else {
                        userobject = JSON.parse(socket.handshake['query']['userobject'])
                    }
                }
            }
        }
  
        if (userobject) userJoin(socket, userobject) //join room user yg baru terhubung

        console.log('a user socket connected')

        socket.on("join", (data) =>{      
            io.in(data._id).emit("forcelogout", "Anda Telah melakukan login pada Device Lain")
            userJoin(socket, data)
        })

        socket.on("news-web", (data)=> {
            console.log(' NEWS WEB DI JALANKAN ?', data)
            io.emit("news", data)
            io.in('global').emit('news', data)
        })

        //contoh penggunaan di web :
        //socket.emit('forcelogout-web', 'user _id yg akan di logout')
        socket.on("forcelogout-web", (target)=>{
            io.in(target).emit("forcelogout", 'Terjadi Pembaruan Data Informasi Akun !')
        })    

        socket.on("leave", (data) =>{      
            console.log('ada yg leave')
      
            userLeave(socket, data)
        })

        socket.on("add wo", (target, data) =>{
            console.log('add wo', target, data)
            // io.emit("add wo", data)
            io.in(target).emit('add wo', data)
            io.in('global').emit('add wo', data)
        })

        socket.on("Answer CC", (target, data) =>{
            console.log('Answer CC', target, data)
            // io.emit("Answer CC", data);
            io.in(target).emit('Answer CC', data)
            io.in('global').emit('Answer CC', data)
        })

        socket.on('disconnect', function() {
            console.log('ada user yg disconnect')    
        })
    })

    function userJoin(socket, userobject) {
        const spvroom = (userobject.supervisor) ? userobject.supervisor : userobject._id
        socket.join('all-'+spvroom) // room (broad cast di bawah supervisor)
        socket.join('all-'+userobject._id)    //room bila dia sebagai spv
        socket.join(userobject._id)        // japri
        socket.join(userobject.userid)     // japri 
        console.log('user join ', userobject.name, spvroom)
    }

    function userLeave(socket, userobject) {
        const spvroom = (userobject.supervisor) ? userobject.supervisor : userobject._id
        socket.leave('all-'+spvroom)
        socket.join('all-'+userobject._id)  
        socket.leave(userobject.supervisor) 
        socket.leave(userobject._id) 
        socket.leave(userobject.userid)
        console.log('user leave ', userobject.name, spvroom)
    }

}) 